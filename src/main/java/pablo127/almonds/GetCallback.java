package pablo127.almonds;
/**
 * 
 * @author jskrepnek (<a href="https://bitbucket.org/jskrepnek" target="_blank">bitbucket page</a>)
 */
public abstract class GetCallback {
	public abstract void done(ParseObject object);
}
