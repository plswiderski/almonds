package pablo127.almonds;

import java.util.List;

/**
 * 
 * @author jskrepnek (<a href="https://bitbucket.org/jskrepnek" target="_blank">bitbucket page</a>)
 */
public abstract class FindCallback {
	public abstract void done(List<ParseObject> objects, ParseException e);
}
