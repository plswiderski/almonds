package pablo127.almonds;

import java.io.IOException;
import java.util.Hashtable;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

/**
 * doesn't work at the moment
 * @author Paweł Świderski (<a href="http://cv-pabloo.rhcloud.com/" target="_blank">http://cv-pabloo.rhcloud.com/</a>)
 */
public class ParseFacebookUtils {

	private static String appId;
	
	public static void initialize(String appId){
		ParseFacebookUtils.appId = appId; 
	}
	
	public static void logIn(List<String> permissions, LogInCallback callback){
		ParseException e = null;
		ParseUser user = new ParseUser();
		try {
			log(permissions, user);
		} catch (ParseException ee) {
			e = ee;
			user = null;
		}
		callback.done(user, e);
		
	}

	private static void log(List<String> permissions, ParseUser user) throws ParseException {
		// TODO Auto-generated method stub
		try	{
			HttpClient httpclient = new DefaultHttpClient();
			
			HttpPost httppost = new HttpPost(Parse.getParseAPIUrlUsers());
			httppost.addHeader("X-Parse-Application-Id", Parse.getApplicationId());
			httppost.addHeader("X-Parse-REST-API-Key", Parse.getRestAPIKey());
			httppost.addHeader("Content-Type", "application/json");

			httppost.setEntity(new StringEntity(getData().toString()));
			HttpResponse httpresponse = httpclient.execute(httppost);

			ParseResponse response = new ParseResponse(httpresponse);
			if (!response.isFailed()){
				JSONObject jsonResponse = response.getJsonObject();

				if (jsonResponse == null){
					throw response.getException();
				}
				
				/* XXX TODO
				try	{
					setObjectId(jsonResponse.getString("objectId"));
					setCreatedAt(jsonResponse.getString("createdAt"));
					try {
						setSessionToken(jsonResponse.getString("sessionToken"));
					} catch (Exception e) {
						
					}
				} 
				
				catch (JSONException e)	{
					throw new ParseException(
							ParseException.INVALID_JSON,
							"Although Parse reports object successfully saved, the response was invalid.",
							e);
				}
				*/ 
			}else{
				throw response.getException();
			}
		}
		catch (ClientProtocolException e){
			throw ParseResponse.getConnectionFailedException(e);
		}catch (IOException e){
			throw ParseResponse.getConnectionFailedException(e);
		}
	}

	private static Hashtable<String, Object> getData() {
		// TODO Auto-generated method stub
		Hashtable<String, Object> mData = new Hashtable<String, Object>();
		mData.put("authData", getPlatformData());		
		return mData;
	}

	private static Hashtable<String, Object> getPlatformData() {
		Hashtable<String, Object> platform = new Hashtable<String, Object>();
		Hashtable<String, Object> platformData = new Hashtable<String, Object>();
		platform.put("facebook", platformData);
		
		platformData.put("id", appId);
		platformData.put("access_token", "XXX");
		platformData.put("expiration_date", "XXX");
		return platform;
	}
	
}
