package pablo127.almonds;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
/**
 * 
 * @author Paweł Świderski (<a href="http://cv-pabloo.rhcloud.com/" target="_blank">http://cv-pabloo.rhcloud.com/</a>)
 */
public class ParseUserTest {

	@Before
	public void setUp() throws Exception {
		Parse.initialize("B3jrmh2W9yIzYCjOLHLI5tLcApvFl1NJAoiJMnTP", "eHDcUJ9DJJqrIsccHbBUFjb0d3srvtnbAJoQxhLL");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testLogginIn(){
		ParseUser.logIn("test", "test", new LogInCallback() {

			public void done(ParseUser obj, ParseException e) {
				assertNotEquals(null, obj);
				assertEquals(null, e);
				assertEquals("test", obj.getUsername());
				assertEquals("pnktnjyb996sj4p156gjtp4im".length(), obj.getSessionToken().length());
				assertEquals("test@test.pl", obj.getEmail());
				assertEquals("test@test.pl", obj.getString("email"));
			}
		});
	}

	@Test
	public void testRegisterNewUser() {
		final ParseUser user = new ParseUser();
		user.setUsername("usernamie" + Math.round(100*Math.random()));
		user.setPassword("passie");
		user.put("email", "emaily"+ Math.round(100*Math.random())+"@mail.pl");
		user.signUp(new SignUpCallback() {

			public void done(ParseException e) {
				assertEquals(null, e);

				assertNotEquals(null, user.getSessionToken());
				assertEquals("pnktnjyb996sj4p156gjtp4im".length(), user.getSessionToken().length());

			}
		});
	}

	@Test
	public void testChangingInformationsLoggingBySessionToken(){
		ParseUser.logIn("testowy", "testowy", new LogInCallback() {

			public void done(ParseUser obj, ParseException e) {
				assertEquals(null, e);
				assertEquals("testowy", obj.getUsername());
				assertEquals("testowy@testowy.pl", obj.getEmail());
				final String sessionToken = obj.getSessionToken();
				ParseUser.become(sessionToken, new LogInCallback() {

					public void done(ParseUser obj, ParseException e) {						
						assertEquals(null, e);
						assertEquals("testowy", obj.getUsername());
						assertEquals("testowy@testowy.pl", obj.getEmail());

						obj.put("info", "informacje");
						obj.save(new SaveCallback() {

							@Override
							public void done(ParseException e) {
								assertEquals(null, e);
							}
						});	
						obj.put("firstTime", true);
						obj.save(new SaveCallback() {

							@Override
							public void done(ParseException e) {
								assertEquals(null, e);
							}
						});
					}
				});
			}
		});
	}

	@Test
	public void testChangingInformations(){
		ParseUser.logIn("testowy", "testowy", new LogInCallback() {

			public void done(ParseUser obj, ParseException e) {
				assertEquals(null, e);
				assertEquals("testowy", obj.getUsername());
				assertEquals("testowy@testowy.pl", obj.getEmail());

				obj.put("info", "informacje");
				obj.save(new SaveCallback() {

					@Override
					public void done(ParseException e) {
						assertEquals(null, e);
					}
				});	
				obj.put("firstTime", false);
				obj.save(new SaveCallback() {

					@Override
					public void done(ParseException e) {
						assertEquals(null, e);
					}
				});
			}
		});
	}

	@Test
	public void testLogginViaSessionTokenBadUser(){
		ParseUser.become("pnktnjyb996sj4p156gjtp4im", new LogInCallback() {

			public void done(ParseUser obj, ParseException e) {
				assertNotEquals(null, e);
				assertEquals(null, obj);
			}
		});

	}

	/* XXX doesn't work
	@Test
	public void testUserDeletion(){
		final ParseUser user = new ParseUser();
		final String userName = "todelete";
		user.setUsername(userName);
		user.setPassword("xcsfsfS");
		user.setEmail("asdsd@dsa.pl");
		user.put("info", "informacje");
		user.signUp(new SignUpCallback() {

			public void done(ParseException e) {
				assertEquals(null, e);
				String sessionToken = user.getSessionToken();
				loggin(sessionToken, true);
			}

			private void loggin(final String sessionToken, final boolean ok){
				ParseUser.become(sessionToken, new LogInCallback() {

					public void done(ParseUser obj, ParseException e) {
						if(ok == true){						
							assertEquals(null, e);
							assertNotEquals(null, obj);
							assertEquals(userName, obj.getUsername());
							assertEquals("informacje", obj.getString("info"));
							try {
								ParseUser.getCurrentUser().delete();
							} catch (ParseException e1) {
								e1.printStackTrace();
								assertEquals(null, e1);
							}
							loggin(sessionToken, false);
						}else {
							assertNotEquals(null, e);
							assertEquals(null, obj);
						}
					}
				});
			}
		});
	}
	 */

	@Test
	public void testAdditionalInformationInUser(){
		final ParseUser user = new ParseUser();
		final String userName = "additional"+System.currentTimeMillis();
		user.setUsername(userName);
		final String password = "dsadsa";
		user.setPassword(password);
		user.setEmail("test"+System.currentTimeMillis()+"@additional.pl");
		final String additionalInfo = "asdksfjlsdfk dsa kjdskj f";
		user.put("info", additionalInfo);
		user.signUp(new SignUpCallback() {

			public void done(ParseException e) {
				assertEquals(null, e);
				ParseUser.logIn(userName, password, new LogInCallback() {

					public void done(ParseUser obj, ParseException e) {
						assertEquals(null, e);
						assertEquals(user.getUsername(), obj.getUsername());
						assertEquals(user.getEmail(), obj.getEmail());
						assertEquals(user.getString("info"), additionalInfo);
					}
				});
			}
		});
	}

	/**
	 * you have to have email verification ON & don't have the same userName, email in database
	 */
	@Test
	public void testEmailVerificationON(){
		final String username = "verif";
		final String email = "test@testt.pl";
		ParseUser.logIn(username, "test", new LogInCallback() {

			@Override
			public void done(ParseUser user, ParseException e) {
				assertEquals(null, e);

				assertNotEquals(null, user.getSessionToken());
				String sessionToken = user.getSessionToken();
				ParseUser.become(sessionToken, new LogInCallback() {

					public void done(ParseUser obj, ParseException e) {
						assertEquals(null, e);
						assertEquals(username, obj.getUsername());
						assertEquals(email, obj.getEmail());
						try {
							assertEquals(false, obj.isEmailVerified());
						} catch (ParseException e1) {
							assertEquals(null, e1);
						}
					}
				});
			}
		});
	}

	@Test
	public void testLogginViaSessionTokenGoodUser(){
		final String sessionToken =  "nLT4EPFkRrnQyGgRPHh7VVMoc";
		ParseUser.become(sessionToken, new LogInCallback() {

			public void done(ParseUser obj, ParseException e) {
				assertEquals(null, e);
				assertNotEquals(null, obj);
				assertEquals("cooldude6", obj.getUsername());
				assertEquals("dsa@dsa.pl", obj.getEmail());		
				assertEquals(sessionToken, obj.getSessionToken());
			}
		});
	}
}
